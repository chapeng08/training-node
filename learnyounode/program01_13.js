/* Write a program that prints the text "HELLO WORLD" to the console  
(stdout).  
   
  
To make a Node.js program, create a new file with a .js extension and  
start writing JavaScript! Execute your program by running it with the node  
command. e.g.:  
   
   $ node program.js  
   
You can write to the console in the same way as in the browser:  
   
   console.log("text")  
*/

console.log("HELLO WORLD")

/*

Recap :
1-Node programs are javascript files.
2-I run my program using $ node programmename.js
3-I can write to the console the same as in the browser.
4-console.log() prints the contents of the brackets to the console.

*/