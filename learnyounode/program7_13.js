/*

HTTP CLIENT  
   
Write a program that performs an HTTP GET request to a URL provided to you  
as the first command-line argument. Write the String contents of each  
"data" event from the response to a new line on the console (stdout).  
   
For this exercise you will need to use the http core module.  
   
Documentation on the http module can be found by pointing your browser  
here: file:///usr/lib/node_modules/learnyounode/node_apidoc/http.html  
   
The http.get() method is a shortcut for simple GET requests, use it to  
simplify your solution. The first argument to http.get() can be the URL  
you want to GET; provide a callback as the second argument.  
   
Unlike other callback functions, this one has the signature:  
   
  function callback (response) { ... }  
   
Where the response object is a Node Stream object. You can treat Node  
Streams as objects that emit events. The three events that are of most  
interest are: "data", "error" and "end". You listen to an event like so:  
   
  response.on("data", function (data) { ... })  
   
The "data" event is emitted when a chunk of data is available and can be  
processed. The size of the chunk depends upon the underlying data source.  
  
The response object / Stream that you get from http.get() also has a  
setEncoding() method. If you call this method with "utf8", the "data"  
events will emit Strings rather than the standard Node Buffer objects  
which you have to explicitly convert to Strings.

*/


var http = require('http');
/*use a new method get() which takes 2 arguments.*/
http.get(process.argv[2], function (response) {
    response.setEncoding('utf8'); /* Now we need to assign our event handlers to 
the response argument. So you can see the .on() method in this case takes 
2 arguments, the data/error and the function to perform. To make this a complete 
http get request we can add the correct encoding. */
    response.on('data', console.log);
    response.on('error', console.error);
});

/* Recap

1. http: is the required module for http requests.
2. .get: is one http method, which is used for making requests to a server.
3. The .get callback function doesn't follow the same node format that we have 
    previously seen.
4. .on(): assigns our event handlers.
5.  Its good practice to include the used encoding. You can't always get away 
    with ommitting it like many people do in html documents.
*/