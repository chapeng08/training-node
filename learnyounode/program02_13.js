/*
Write a program that accepts one or more numbers as command-line arguments 
and prints the sum of those numbers to the console (stdout).
*/

/*
We don't want to use the first two objects in the array. Because The first 
element of the process.argv array is always 'node', and the second element is 
always the path to your program.js file, so you need to start at the 3rd element
(index 2), adding each item to the total until you reach the end of the array. 
Let's look at the For loop.
*/

var result = 0; 

/*
(cf learn-javascript/Docs/for-loop.md) Ici on déclare des variables avec var 
car non locales dans la loop.
*/
/*
Statement1 is a variable declaration. Statement2 is a condition to run the 
script. Statement3 is something to be done after the script runs. doSomething 
is the script to run if the condition is true.
*/

for (var i = 2; i < process.argv.length; i++) {
    result += Number(process.argv[i]); //+= total
}
     console.log(result)

/*
process.argv.length correspond à la longueur du tableau de tous les arguments 
de ligne de commande. i++ incrémente i de 1 après chaque passage dans la boucle.
*/

/*

Details
1. var i = 2: we have created the variable i which has the starting value of 2. 
We start at value 2 because we want to ignore the first 2 objects in our array 
(node and the file name).

2. i < process.argv.length: The .length function gives us the length of our 
array. So if we had the array 
[ 'node', '/path/to/your/program.js', '1', '2', '3' ] the length would be 5. So
our statement means only run the script if i is less than 5. This has 2 effects.
The first thing is does is ensure that the code only runs when we have passed 
arguments through the command line. If we only type $ node program.js there will
be no numbers and so no need to run the script. The second thing this does is 
give us the ability to stop the For loop.

3. i++: This actually closes the for loop. If you don't close a For loop you can
get an infinite loop. Worst case scenario, you crash your server. i++ means 
add 1 to the value of i and reassign this to i. This is a shortened version of 
i = i + 1. So if the condition is true, the script is run, then statement3 runs. 
When the final number is added the value of i will be the same as the length of 
the array, meaning the condition in statement2 is no longer true and the 
loop stops.

((Also be aware that all elements of process.argv are strings and you may need 
to coerce them into numbers. You can do this by prefixing the property with + 
or passing it to Number(). e.g. +process.argv[2] or Number(process.argv[2]).))

4. result += Number(process.argv[i])
  result: an undefined variable
  +=: add and reassign. So the number on the right hand side is added to the 
value of the variable on the left hand side. Then the new value is given to the 
variable. For example: if my variable shoes has a value of 3, then the line 
shoes += 4 will change the value to 7.
  Number(): converts the contents of the brackets into its number equivelant. 
For example Number("4") gives me the value 4, Number(false) gives me the 
value 0. This is necessary as the arguments of process.argv are not numbers, 
they are strings.
  process.argv[i]: The object in position i of the array. This means that 
depending on how many times the For loop runs, the value of i will reflect a 
different object inside the array.

*/
