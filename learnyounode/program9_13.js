/* This problem is the same as the previous problem (HTTP COLLECT) in that  
you need to use http.get(). However, this time you will be provided with  
three URLs as the first three command-line arguments.  
   
You must collect the complete content provided to you by each of the URLs  
and print it to the console (stdout). You don't need to print out the  
length, just the data as a String; one line per URL. The catch is that you  
must print them out in the same order as the URLs are provided to you as  
command-line arguments. 

Don't expect these three servers to play nicely! They are not going to  
give you complete responses in the order you hope, so you can't naively  
just print the output as you get it because they will be out of order.  
   
You will need to queue the results and keep track of how many of the URLs  
have returned their entire contents. Only once you have them all, you can  
print the data to the console.  
   
Counting callbacks is one of the fundamental ways of managing async in  
Node. Rather than doing it yourself, you may find it more convenient to  
rely on a third-party library such as [async](https://npmjs.com/async) or  
[after](https://npmjs.com/after). But for this exercise, try and do it  
without any external helper library. */

/* First, we need our bl and http modules */

var http = require('http')
var bl = require('bl')

/*
We are going to need 2 main functions. 
One is to stream the data from the 3 urls : "httpGet" 
and the other "printResults" is to print out the data once it has been recieved. 
Seeing as the httpGet will be calling on printResults we should write 
printResults out first. 
*/

/*
We can write our function to print out the elements of an array one line at 
a time. So our array can be a variable, lets call it results. The results 
variable will contain the data from each url. Then we can use a for loop to 
print out the results. So first create a variable and assign it an empty array.
*/

var results = []
var count = 0

function printResults () {
  for (var i = 0; i < 3; i++) {
    console.log(results[i])
  }
}
/*
We now need to write out httpGet function. We need to open a stream for each 
url, collect the data and then store it in the results array. How can we do this
to all 3? We can create a for loop to go through each url, collect the data, and
then add it to the corresponding position in the array.
*/

function httpGet (index) {
  http.get(process.argv[2 + index], function (response) {
/*
We now perform a http.get on each url in the command line. Now pipe the stream 
to collect the data, incuding the early error callback.
*/
    response.pipe(bl(function (err, data) {
      if (err) {
        return console.error(err)
    }
/*
Now we need to string the data, and add it to the results array in the 
corresponding position. We can do this using our index.
*/
      results[index] = data.toString()
/*
Our program is storing the data from the 3 urls in our command line. We need to 
now somehow use our printResults function to print out the data. But it has to 
all be done in the correct order, all at once. So we need to wait for all the 
data to be collected and stored. We can use a count mechanism to do this.
var count = 0
Then we add instructions to increase the count by 1 when the data has been 
stringed and added to the results array.
*/
      count++
/*
Now we need to carryout our printResuts function when all the data is stored. So
at 3 count.
*/
    if (count == 3) {
      printResults() /* Our printResults function now loops through our results 
      array, printing off each element which is a string of data from the 
      corresponding urls in the same order they were passed to the console. */
      }
    }))
  })
}

for (var i = 0; i < 3; i++) {
  httpGet(i)
}