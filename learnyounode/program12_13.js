/* 
Write an HTTP server that receives only POST requests and converts  
incoming POST body characters to upper-case and returns it to the client.  
   
Your server should listen on the port provided by the first argument to  
your program.  

While you're not restricted to using the streaming capabilities of the  
request and response objects, it will be much easier if you do.  
   
There are a number of different packages in npm that you can use to  
"transform" stream data as it's passing through. For this exercise the  
through2-map package offers the simplest API.  

through2-map allows you to create a transform stream using only a single  
function that takes a chunk of data and returns a chunk of data. It's  
designed to work much like Array#map() but for streams

  var map = require('through2-map')  
     inStream.pipe(map(function (chunk) {  
       return chunk.toString().split('').reverse().join('')  
     })).pipe(outStream)

In the above example, the incoming data from inStream is converted to a  
String (if it isn't already), the characters are reversed and the result  
is passed through to outStream. So we've made a chunk character reverser!  
Remember though that the chunk size is determined up-stream and you have  
little control over it for incoming data.

To install through2-map type:  
   
   $ npm install through2-map  
   
If you don't have an Internet connection, simply make a node_modules  
directory and copy the entire directory for the module you want to use  
from inside the learnyounode installation directory:  
   
file:///usr/lib/node_modules/learnyounode/node_modules/through2-map  
   
Documentation for through2-map has been installed along with learnyounode  
on your system and you can read them by pointing your browser here:  
   
file:///usr/lib/node_modules/learnyounode/docs/through2-map.html
*/

/* The http.createServer() method will be used again so load the http module into
the variable http. */
var http = require('http');
/*We use a package called through2-map that will create a transform stream. For
now, load it into a variable called map.*/
var map = require('through2-map');
/* There is a port number that has been supplied as the first command line 
argument. We Assign that to the variable port. */
var port = process.argv[2]
/* we use http.createServer() to listen on the port. In the callback function, 
we detect if the request method is POST. If so, we pipe the data and as the 
destination, we pass the map() function as the first argument. We pass a 
function a an argument to map() and within that argument, simply we return the
chunk as an upper cased string. */
http.createServer(function (req, res) {
if (req.method === `POST`) {
req.pipe(map(function (chunk) {
return chunk.toString().toUpperCase();
})).pipe(res);
}
}).listen(port);