

var fs = require('fs');
var path = require('path');

module.exports = function (dir, filterStr, callback) {
/* the module export a single function that takes three arguments: the directory
name, the filename extension string and a callback function, in that order.*/
    fs.readdir(dir, function (err, list) {
        if (err) {
            return callback(err);
        }
 
        list = list.filter(function (file) {
            return path.extname(file) === '.' + filterStr
        });
 
        callback(null, list)
    })
}
