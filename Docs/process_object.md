You can access command-line arguments via the global process object. The  process object has an argv property which is an array containing the complete command-line. i.e. process.argv.  
To get started, write a program that simply contains:  
   
     console.log(process.argv)  
   
Run it with node program.js and some numbers as arguments. e.g:  
   
     $ node program.js 1 2 3  
   
In which case the output would be an array looking something like:  
   
     [ 'node', '/path/to/your/program.js', '1', '2', '3' ] 

The first element of the process.argv array is always 'node', and the second element is always the path to your program.js file, so you need to start at the 3rd element (index 2).

Also be aware that all elements of process.argv are strings and you may need to coerce them into numbers. You can do this by prefixing the property with + or passing it to Number(). e.g. +process.argv[2] or Number(process.argv[2]).