This step adds one of the main ideas of Node.js, and that is asynchronous operations. This program needs to use just one asynchronous operation to read a file and provide the count of newlines it has to the console. We will need to make use of fs.readFile() and Callbacks for this.

fs.readFile(path[, options], callback)

    path <string> | <Buffer> | <URL> | <integer> filename or file descriptor

    options <Object> | <string>
        encoding <string> | <null> Default: null
        flag <string> See support of file system flags. Default: 'r'.

    callback <Function>
        err <Error>
        data <string> | <Buffer>


Asynchronously reads the entire contents of a file.

fs.readFile('/etc/passwd', (err, data) => {
  if (err) throw err;
  console.log(data);
});

The callback is passed two arguments (err, data), where data is the contents of the file.
If no encoding is specified, then the raw buffer is returned.
If options is a string, then it specifies the encoding:

fs.readFile('/etc/passwd', 'utf8', callback);

The asynchronous form always takes a completion callback as its last argument. The arguments passed to the completion callback depend on the method, but the first argument is always reserved for an exception. If the operation was completed successfully, then the first argument will be null or undefined.

