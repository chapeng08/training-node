ArrayList forEach()

ArrayList forEach() method performs the argument statement/action for each element of the list until all elements have been processed or the action throws an exception.
By default, actions are performed on elements taken in the order of iteration.

