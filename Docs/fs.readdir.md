fs.readdir(path[, options], callback)

path <string> | <Buffer> | <URL>

  options <string> | <Object>
        encoding <string> Default: 'utf8'
        withFileTypes <boolean> Default: false

  callback <Function>
        err <Error>
        files <string[]> | <Buffer[]> | <fs.Dirent[]>

Asynchronous readdir(3). Reads the contents of a directory. The callback gets two arguments (err, files) where files is an array of the names of the files in the directory excluding '.' and '..'.
The optional options argument can be a string specifying an encoding, or an object with an encoding property specifying the character encoding to use for the filenames passed to the callback. If the encoding is set to 'buffer', the filenames returned will be passed as Buffer objects.
If options.withFileTypes is set to true, the files array will contain fs.Dirent objects.

